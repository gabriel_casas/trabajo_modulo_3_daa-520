#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 16:57:35 2020

@author: gcasas
"""
from _DataProcess import *
from _Unet import *

model = unetModel().unet()

model.load_weights('./outModel/unet_modelOk.hdf5')
testGene = testGenerator("./dataset/test", num_image=12)
results = model.predict_generator(testGene, 12, verbose=1)

saveResult("./outModel/pred", results)
print('Images predicted was saved into outModel/pred')