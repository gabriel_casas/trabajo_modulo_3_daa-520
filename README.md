# Unet
### Gabriel Casas M. 
Machine Learning Dip. UCB 

## trabajo_modulo_2_DAA-520
U-net: Convolutional networks for biomedical image segmentation

Los detalles del trabajo se encuentran en el archivo **reproducibilidad.pdf**

Se trabajo mas en la implementacion del modelo, las pruebas con diferentes tipos de dataset y las comparaciones de los resultados.

Losdataset utilizados se describen en el documento **reproducibilidad.pdf** se adjuntan tambien los mejores resltados de las predicciones obtenidas.

La ejecucion esta disponible en colab:

https://colab.research.google.com/drive/14hnExkrr0B5zyOG5ohqf0S08TYbyB_dO

crear una carpeta llamada **unet** en drive
donde se debe copiar toda la carpeta **dataset** crear la carpeta **outModel**
ademas se debe subir los archivos **_DataProcess.py _TrainingUnet.py _Unet.py**

