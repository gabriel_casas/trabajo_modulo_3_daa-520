## References of UNET

* https://arxiv.org/pdf/1505.04597.pdf
* https://towardsdatascience.com/understanding-semantic-segmentation-with-unet-6be4f42d4b47
* https://lmb.informatik.uni-freiburg.de/people/ronneber/u-net/
* https://www.kaggle.com/micajoumathematics/my-first-semantic-segmentation-keras-u-net
* https://github.com/jocicmarko/ultrasound-nerve-segmentation/blob/master/train.py
* https://www.kaggle.com/c/ultrasound-nerve-segmentation/overview
* https://www.kaggle.com/joydeepmedhi/train-py
* https://www.kaggle.com/vbookshelf/computed-tomography-ct-images

