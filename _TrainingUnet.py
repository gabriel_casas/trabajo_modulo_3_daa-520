#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 23:27:13 2020

@author: gcasas
"""

from _DataProcess import *
from _Unet import *
from keras.callbacks import ModelCheckpoint

data_gen_args = dict(rotation_range=0.2,
                    width_shift_range=0.05,
                    height_shift_range=0.05,
                    shear_range=0.05,
                    zoom_range=0.05,
                    horizontal_flip=True,
                    fill_mode='nearest')
myGene = trainGenerator(2, './dataset/train', 'image1', 'masks1', data_gen_args, save_to_dir = None)

model = unetModel().unet()
model_checkpoint = ModelCheckpoint('./outModel/unet_model.hdf5', monitor='loss',verbose=1, save_best_only=True)
model.fit_generator(myGene,steps_per_epoch=350,epochs=1,callbacks=[model_checkpoint])

print('The unet`s model was saved into outModel/unet_model.hdf5')
